package net.rubywillow.bigmath

import net.rubywillow.bigmath.BigDecimalMath.bernoulli
import net.rubywillow.bigmath.BigDecimalMath.factorial
import net.rubywillow.bigmath.BigDecimalMath.toBigDecimal
import org.testng.annotations.Test
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode
import java.util.*
import kotlin.concurrent.thread
import kotlin.math.*
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail


enum class TestLevel {
    Fast, Medium, Slow
}

class TestBigDecimalMath {

    private val MC_CHECK_DOUBLE = MathContext(10)
    private val TEST_LEVEL: TestLevel = getTestLevel()
    private val MC = MathContext.DECIMAL128
    private val AUTO_TEST_MAX_PRECISION: Int = getMaxPrecision()
    private val RANDOM_MAX_PRECISION: Int = getMaxPrecision()


    @Test
    fun testInternals() {
        assertEquals(null, toCheck(Double.NaN))
        assertEquals(null, toCheck(Double.NEGATIVE_INFINITY))
        assertEquals(null, toCheck(Double.POSITIVE_INFINITY))
        assertBigDecimal(BigDecimal("1.23"), BigDecimal("1.23"), MathContext(3))
        assertBigDecimal(BigDecimal("1.23"), BigDecimal("1.23"), MathContext(2))
        assertBigDecimal(BigDecimal("1.23"), BigDecimal("1.23"), MathContext(1))
        assertBigDecimal(BigDecimal("1.24"), BigDecimal("1.23"), MathContext(3))
        assertBigDecimal(BigDecimal("1.23"), BigDecimal("1.24"), MathContext(3))
        assertThrows(IllegalArgumentException::class.java) { throw IllegalArgumentException() }
        assertThrows(IllegalArgumentException::class.java, "blabla") { throw IllegalArgumentException("blabla") }
    }

    @Test(expectedExceptions = [AssertionError::class])
    fun testInternalsAssertBigDecimalFail1() {
        assertBigDecimal(BigDecimal("1.25"), BigDecimal("1.23"), MathContext(3))
    }

    @Test(expectedExceptions = [AssertionError::class])
    fun testInternalsBigDecimalFail2() {
        assertBigDecimal(BigDecimal("1.23"), BigDecimal("1.25"), MathContext(3))
    }

    @Test(expectedExceptions = [AssertionError::class])
    fun testInternalsAssertThrowsFail1() {
        assertThrows(IllegalArgumentException::class.java) {}
    }

    @Test(expectedExceptions = [AssertionError::class])
    fun testInternalsAssertThrowsFail2() {
        assertThrows(IllegalArgumentException::class.java) { throw RuntimeException() }
    }

    @Test(expectedExceptions = [AssertionError::class])
    fun testInternalsAssertThrowsFail3() {
        assertThrows(
            IllegalArgumentException::class.java,
            "blabla"
        ) { throw IllegalArgumentException("another message") }
    }

    @Test
    fun testToBigDecimalFails() {
        assertToBigDecimalThrows("")
        assertToBigDecimalThrows("x")
        assertToBigDecimalThrows(" ")
        assertToBigDecimalThrows("1x")
        assertToBigDecimalThrows("1.2x")
        assertToBigDecimalThrows("1.2E3x")
        assertToBigDecimalThrows("1..2")
        assertToBigDecimalThrows("1.2.3")
        assertToBigDecimalThrows("++1")
        assertToBigDecimalThrows("+1+2")
        assertToBigDecimalThrows("--1")
        assertToBigDecimalThrows("-1-2")
        assertToBigDecimalThrows("+-1")
        assertToBigDecimalThrows("+1-2")
        assertToBigDecimalThrows("-+1")
        assertToBigDecimalThrows("-1+2")
        assertToBigDecimalThrows("1EE2")
        assertToBigDecimalThrows("1E2.3")
        assertToBigDecimalThrows("1E++2")
        assertToBigDecimalThrows("1E+2+3")
        assertToBigDecimalThrows("1E--2")
        assertToBigDecimalThrows("1E-2-3")
        assertToBigDecimalThrows("1E+-2")
        assertToBigDecimalThrows("1E+2-3")
        assertToBigDecimalThrows("1E-+2")
        assertToBigDecimalThrows("1E-2+3")
        assertToBigDecimalThrows("1E9999999999")
        assertToBigDecimalThrows("1E999999999999999")
    }

    @Test
    fun testToBigDecimal() {
        assertToBigDecimal("0")
        assertToBigDecimal("00")
        assertToBigDecimal("0.0")
        assertToBigDecimal("0.00")
        assertToBigDecimal("00.00")
        assertToBigDecimal("+0")
        assertToBigDecimal("-0")
        assertToBigDecimal("+0E123")
        assertToBigDecimal("-0E-123")
        assertToBigDecimal(".123")
        assertToBigDecimal("123.")
        assertToBigDecimal("1")
        assertToBigDecimal("1.0")
        assertToBigDecimal("1.23")
        assertToBigDecimal("1.2300")
        assertToBigDecimal("1234567890")
        assertToBigDecimal("1234567890.1234567890123456789")
        assertToBigDecimal("001")
        assertToBigDecimal("001.23")
        assertToBigDecimal("001.2300")
        assertToBigDecimal("001234567890")
        assertToBigDecimal("001234567890.1234567890123456789")
        assertToBigDecimal("+1")
        assertToBigDecimal("+1.23")
        assertToBigDecimal("+1.2300")
        assertToBigDecimal("+1234567890")
        assertToBigDecimal("+1234567890.1234567890123456789")
        assertToBigDecimal("-1")
        assertToBigDecimal("-1.23")
        assertToBigDecimal("-1.2300")
        assertToBigDecimal("-1234567890")
        assertToBigDecimal("-1234567890.1234567890123456789")
        assertToBigDecimal("1.23E123")
        assertToBigDecimal("1.2300E123")
        assertToBigDecimal("1.23E+123")
        assertToBigDecimal("1.23E-123")
    }

    @Test
    fun testIsIntValue() {
        assertEquals(true, BigDecimal.valueOf(Int.MIN_VALUE.toLong()).isIntValue())
        assertEquals(true, BigDecimal.valueOf(Int.MAX_VALUE.toLong()).isIntValue())
        assertEquals(true, BigDecimal.valueOf(0).isIntValue())
        assertEquals(true, BigDecimal.valueOf(-55).isIntValue())
        assertEquals(true, BigDecimal.valueOf(33).isIntValue())
        assertEquals(true, BigDecimal.valueOf(-55.0).isIntValue())
        assertEquals(true, BigDecimal.valueOf(33.0).isIntValue())
        assertEquals(
            false,
            BigDecimal.valueOf(Int.MIN_VALUE.toLong()).subtract(BigDecimal.ONE).isIntValue()
        )
        assertEquals(false, BigDecimal.valueOf(Int.MAX_VALUE.toLong()).add(BigDecimal.ONE).isIntValue())
        assertEquals(false, BigDecimal.valueOf(3.333).isIntValue())
        assertEquals(false, BigDecimal.valueOf(-5.555).isIntValue())
    }

    @Test
    fun testIsLongValue() {
        assertEquals(true, BigDecimal.valueOf(Long.MIN_VALUE).isLongValue())
        assertEquals(true, BigDecimal.valueOf(Long.MAX_VALUE).isLongValue())
        assertEquals(true, BigDecimal.valueOf(0).isLongValue())
        assertEquals(true, BigDecimal.valueOf(-55).isLongValue())
        assertEquals(true, BigDecimal.valueOf(33).isLongValue())
        assertEquals(true, BigDecimal.valueOf(-55.0).isLongValue())
        assertEquals(true, BigDecimal.valueOf(33.0).isLongValue())
        assertEquals(false, BigDecimal.valueOf(Long.MIN_VALUE).subtract(BigDecimal.ONE).isLongValue())
        assertEquals(false, BigDecimal.valueOf(Long.MAX_VALUE).add(BigDecimal.ONE).isLongValue())
        assertEquals(false, BigDecimal.valueOf(3.333).isLongValue())
        assertEquals(false, BigDecimal.valueOf(-5.555).isLongValue())
    }

    @Test
    fun testIsDoubleValue() {
        assertEquals(true, BigDecimal.valueOf(Double.MIN_VALUE).isDoubleValue())
        assertEquals(true, BigDecimal.valueOf(Double.MAX_VALUE).isDoubleValue())
        assertEquals(true, BigDecimal.valueOf(-Double.MAX_VALUE).isDoubleValue())
        assertEquals(true, BigDecimal.valueOf(-Double.MIN_VALUE).isDoubleValue())
        assertEquals(true, BigDecimal.valueOf(0).isDoubleValue())
        assertEquals(true, BigDecimal.valueOf(-55).isDoubleValue())
        assertEquals(true, BigDecimal.valueOf(33).isDoubleValue())
        assertEquals(true, BigDecimal("1E-325").isDoubleValue())
        assertEquals(true, BigDecimal("-1E-325").isDoubleValue())
        assertEquals(true, BigDecimal("1E-325").isDoubleValue())
        assertEquals(true, BigDecimal("-1E-325").isDoubleValue())
        assertEquals(false, BigDecimal.valueOf(Double.MAX_VALUE).add(BigDecimal.valueOf(1)).isDoubleValue())
        assertEquals(false, BigDecimal.valueOf(-Double.MAX_VALUE).subtract(BigDecimal.valueOf(1)).isDoubleValue())
        assertEquals(false, BigDecimal("1E309").isDoubleValue())
        assertEquals(false, BigDecimal("-1E309").isDoubleValue())
    }

    @Test
    fun testMantissa() {
        assertEquals(0, BigDecimal.ZERO.compareTo(BigDecimal.ZERO.mantissa()))
        assertEquals(0, BigDecimal("1.2345").compareTo(BigDecimal("123.45").mantissa()))
        assertEquals(0, BigDecimal("1.2345").compareTo(BigDecimal("0.00012345").mantissa()))
        assertEquals(0, BigDecimal("1.2345").compareTo(BigDecimal("123.45").mantissa()))
        assertEquals(0, BigDecimal("1.2345").compareTo(BigDecimal("0.00012345").mantissa()))
        assertEquals(0, BigDecimal("1.2345").compareTo(BigDecimal("123.45000").mantissa()))
        assertEquals(0, BigDecimal("1.2345").compareTo(BigDecimal("0.00012345000").mantissa()))
        assertEquals(0, BigDecimal("1.2345").compareTo(BigDecimal("12.345E1").mantissa()))
        assertEquals(0, BigDecimal("1.2345").compareTo(BigDecimal("0.0012345E-1").mantissa()))
        assertEquals(-1, BigDecimal("1.2345").compareTo(BigDecimal("123.459999").mantissa()))
        assertEquals(1, BigDecimal("1.2345").compareTo(BigDecimal("0.000123").mantissa()))
    }

    @Test
    fun testExponent() {
        assertEquals(0, BigDecimal.ZERO.exponent())
        assertEquals(0, BigDecimal("1.2345").exponent())
        assertEquals(2, BigDecimal("123.45").exponent())
        assertEquals(-2, BigDecimal("0.012345").exponent())
        assertEquals(0, BigDecimal("123.45E-2").exponent())
        assertEquals(0, BigDecimal("0.012345E2").exponent())
        assertEquals(3, BigDecimal("1.2345E3").exponent())
        assertEquals(5, BigDecimal("123.45E3").exponent())
        assertEquals(1, BigDecimal("0.012345E3").exponent())
    }

    @Test
    fun testSignificantDigits() {
        assertEquals(1, BigDecimal.ZERO.significantDigits())
        assertEquals(3, BigDecimal("123").significantDigits())
        assertEquals(3, BigDecimal("12.3").significantDigits())
        assertEquals(3, BigDecimal("1.23").significantDigits())
        assertEquals(3, BigDecimal("0.123").significantDigits())
        assertEquals(3, BigDecimal("0.0123").significantDigits())
        assertEquals(3, BigDecimal("0.00123").significantDigits())
        assertEquals(3, BigDecimal("12.300").significantDigits())
        assertEquals(3, BigDecimal("1.2300").significantDigits())
        assertEquals(3, BigDecimal("0.12300").significantDigits())
        assertEquals(6, BigDecimal("123000").significantDigits())
        assertEquals(6, BigDecimal("123000.00").significantDigits())
        assertEquals(3, BigDecimal("-123").significantDigits())
        assertEquals(3, BigDecimal("-12.3").significantDigits())
        assertEquals(3, BigDecimal("-1.23").significantDigits())
        assertEquals(3, BigDecimal("-0.123").significantDigits())
        assertEquals(3, BigDecimal("-0.0123").significantDigits())
        assertEquals(3, BigDecimal("-0.00123").significantDigits())
        assertEquals(3, BigDecimal("-12.300").significantDigits())
        assertEquals(3, BigDecimal("-1.2300").significantDigits())
        assertEquals(3, BigDecimal("-0.12300").significantDigits())
        assertEquals(6, BigDecimal("-123000").significantDigits())
        assertEquals(6, BigDecimal("-123000.00").significantDigits())
    }

    @Test
    fun testIntegralPart() {
        assertEquals(0, BigDecimal.ZERO.compareTo(BigDecimal.ZERO.integralPart()))
        assertEquals(0, BigDecimal("1").compareTo(BigDecimal("1.2345").integralPart()))
        assertEquals(0, BigDecimal("123").compareTo(BigDecimal("123.45").integralPart()))
        assertEquals(0, BigDecimal("0").compareTo(BigDecimal("0.12345").integralPart()))
        assertEquals(0, BigDecimal("-1").compareTo(BigDecimal("-1.2345").integralPart()))
        assertEquals(0, BigDecimal("-123").compareTo(BigDecimal("-123.45").integralPart()))
        assertEquals(0, BigDecimal("-0").compareTo(BigDecimal("-0.12345").integralPart()))
        assertEquals(0, BigDecimal("123E987").compareTo(BigDecimal("123E987").integralPart()))
        assertEquals(0, BigDecimal("0").compareTo(BigDecimal("123E-987").integralPart()))
    }

    @Test
    fun testFractionalPart() {
        assertEquals(0, BigDecimal.ZERO.compareTo(BigDecimal.ZERO.fractionalPart()))
        assertEquals(0, BigDecimal("0.2345").compareTo(BigDecimal("1.2345").fractionalPart()))
        assertEquals(0, BigDecimal("0.45").compareTo(BigDecimal("123.45").fractionalPart()))
        assertEquals(0, BigDecimal("0.12345").compareTo(BigDecimal("0.12345").fractionalPart()))
        assertEquals(0, BigDecimal("-0.2345").compareTo(BigDecimal("-1.2345").fractionalPart()))
        assertEquals(0, BigDecimal("-0.45").compareTo(BigDecimal("-123.45").fractionalPart()))
        assertEquals(0, BigDecimal("-0.12345").compareTo(BigDecimal("-0.12345").fractionalPart()))
        assertEquals(0, BigDecimal("0").compareTo(BigDecimal("123E987").fractionalPart()))
        assertEquals(0, BigDecimal("123E-987").compareTo(BigDecimal("123E-987").fractionalPart()))
    }

    @Test
    fun testRoundWithTrailingZeroes() {
        val mc = MathContext(5)
        assertEquals("0.0000", BigDecimal("0").roundWithTrailingZeroes(mc).toString())
        assertEquals("0.0000", BigDecimal("0.00000000000").roundWithTrailingZeroes(mc).toString())
        assertEquals("1.2345", BigDecimal("1.2345").roundWithTrailingZeroes(mc).toString())
        assertEquals("123.45", BigDecimal("123.45").roundWithTrailingZeroes(mc).toString())
        assertEquals("0.0012345", BigDecimal("0.0012345").roundWithTrailingZeroes(mc).toString())
        assertEquals("1.2346", BigDecimal("1.234567").roundWithTrailingZeroes(mc).toString())
        assertEquals("1.2300", BigDecimal("1.23").roundWithTrailingZeroes(mc).toString())
        assertEquals("1.2300", BigDecimal("1.230000").roundWithTrailingZeroes(mc).toString())
        assertEquals("123.46", BigDecimal("123.4567").roundWithTrailingZeroes(mc).toString())
        assertEquals("0.0012346", BigDecimal("0.001234567").roundWithTrailingZeroes(mc).toString())
        assertEquals("0.0012300", BigDecimal("0.00123").roundWithTrailingZeroes(mc).toString())
        assertEquals("1.2346E+100", BigDecimal("1.234567E+100").roundWithTrailingZeroes(mc).toString())
        assertEquals("1.2346E-100", BigDecimal("1.234567E-100").roundWithTrailingZeroes(mc).toString())
        assertEquals("1.2300E+100", BigDecimal("1.23E+100").roundWithTrailingZeroes(mc).toString())
        assertEquals("1.2300E-100", BigDecimal("1.23E-100").roundWithTrailingZeroes(mc).toString())
        assertEquals("1.2346E+999999999", BigDecimal("1.234567E+999999999").roundWithTrailingZeroes(mc).toString())
        assertEquals("1.2346E-999999999", BigDecimal("1.234567E-999999999").roundWithTrailingZeroes(mc).toString())
        assertEquals("1.2300E+999999999", BigDecimal("1.23E+999999999").roundWithTrailingZeroes(mc).toString())
        assertEquals("1.2300E-999999999", BigDecimal("1.23E-999999999").roundWithTrailingZeroes(mc).toString())
    }

    @Test
    fun testE() {
        // Result from wolframalpha.com: exp(1)
        val expected = BigDecimal(
            "2.7182818284590452353602874713526624977572470936999595749669676277240766303535475945713821785251664274274663919320030599218174135966290435729003342952605956307381323286279434907632338298807531952510190115738341879307021540891499348841675092447614606680822648001684774118537423454424371075390777449920695517027618386062613313845830007520449338265602976067371132007093287091274437470472306969772093101416928368190255151086574637721112523897844250569536967707854499699679468644549059879316368892300987931277361782154249992295763514822082698951936680331825288693984964651058209392398294887933203625094431173012381970684161403970198376793206832823764648042953118023287825098194558153017567173613320698112509961818815930416903515988885193458072738667385894228792284998920868058257492796104841984443634632449684875602336248270419786232090021609902353043699418491463140934317381436405462531520961836908887070167683964243781405927145635490613031072085103837505101157477041718986106873969655212671546889570350354021234078498193343210681701210056278802351930332247450158539047304199577770935036604169973297250886876966403555707162268447162560798826517871341951246652010305921236677194325278675398558944896970964097545918569563802363701621120477427228364896134225164450781824423529486363721417402388934412479635743702637552944483379980161254922785092577825620926226483262779333865664816277251640191059004916449982893150566047258027786318641551956532442586982946959308019152987211725563475463964479101459040905862984967912874068705048958586717479854667757573205681288459205413340539220001137863009455606881667400169842055804033637953764520304024322566135278369511778838638744396625322498506549958862342818997077332761717839280349465014345588970719425863987727547109629537415211151368350627526023264847287039207643100595841166120545297030"
        )
        assertPrecisionCalculation(
            expected,
            { mathContext -> e(mathContext) },
            10
        )
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testEUnlimitedFail() {
        e(MathContext.UNLIMITED)
    }

    @Test
    fun testPi() {
        // Result from wolframalpha.com: pi
        val expected = BigDecimal(
            "3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821480865132823066470938446095505822317253594081284811174502841027019385211055596446229489549303819644288109756659334461284756482337867831652712019091456485669234603486104543266482133936072602491412737245870066063155881748815209209628292540917153643678925903600113305305488204665213841469519415116094330572703657595919530921861173819326117931051185480744623799627495673518857527248912279381830119491298336733624406566430860213949463952247371907021798609437027705392171762931767523846748184676694051320005681271452635608277857713427577896091736371787214684409012249534301465495853710507922796892589235420199561121290219608640344181598136297747713099605187072113499999983729780499510597317328160963185950244594553469083026425223082533446850352619311881710100031378387528865875332083814206171776691473035982534904287554687311595628638823537875937519577818577805321712268066130019278766111959092164201989380952572010654858632788659361533818279682303019520353018529689957736225994138912497217752834791315155748572424541506959508295331168617278558890750983817546374649393192550604009277016711390098488240128583616035637076601047101819429555961989467678374494482553797747268471040475346462080466842590694912933136770289891521047521620569660240580381501935112533824300355876402474964732639141992726042699227967823547816360093417216412199245863150302861829745557067498385054945885869269956909272107975093029553211653449872027559602364806654991198818347977535663698074265425278625518184175746728909777727938000816470600161452491921732172147723501414419735685481613611573525521334757418494684385233239073941433345477624168625189835694855620992192221842725502542568876717904946016534668049886272327917860857843838279679766814541009538"
        )
        assertPrecisionCalculation(
            expected,
            { mathContext -> pi(mathContext) },
            10
        )
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testPiUnlimitedFail() {
        pi(MathContext.UNLIMITED)
    }

    @Test
    fun testBernoulli() {
        assertBigDecimal(toCheck(1.0)!!, bernoulli(0, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(-1.0 / 2)!!, bernoulli(1, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(1.0 / 6)!!, bernoulli(2, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(0.0)!!, bernoulli(3, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(-1.0 / 30)!!, bernoulli(4, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(0.0)!!, bernoulli(5, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(1.0 / 42)!!, bernoulli(6, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(0.0)!!, bernoulli(7, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(-1.0 / 30)!!, bernoulli(8, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(0.0)!!, bernoulli(9, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(5.0 / 66)!!, bernoulli(10, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(0.0)!!, bernoulli(11, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(-691.0 / 2730)!!, bernoulli(12, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(0.0)!!, bernoulli(13, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(7.0 / 6)!!, bernoulli(14, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(0.0)!!, bernoulli(15, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(-3617.0 / 510)!!, bernoulli(16, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(0.0)!!, bernoulli(17, MC), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(43867.0 / 798)!!, bernoulli(18, MC), MC_CHECK_DOUBLE)
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testBernoulliNegative() {
        bernoulli(-1, MC)
    }

    @Test
    fun testBernoulliUnlimited() {
        assertBigDecimal(toCheck(1.0)!!, bernoulli(0, MathContext.UNLIMITED), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(-1.0 / 2)!!, bernoulli(1, MathContext.UNLIMITED), MC_CHECK_DOUBLE)
        assertBigDecimal(toCheck(1.0)!!, bernoulli(3, MathContext.UNLIMITED), MC_CHECK_DOUBLE)
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testBernoulliUnlimitedFail() {
        bernoulli(2, MathContext.UNLIMITED)
    }

    @Test
    fun testReciprocal() {
        assertEquals(BigDecimal.ONE, BigDecimal.valueOf(1).reciprocal(MC))
        assertEquals(BigDecimal.valueOf(0.5), BigDecimal.valueOf(2).reciprocal(MC))
    }

    @Test
    fun testReciprocalUnlimited() {
        assertEquals(BigDecimal.ONE, BigDecimal.valueOf(1).reciprocal(MathContext.UNLIMITED))
        assertEquals(BigDecimal.valueOf(0.5), BigDecimal.valueOf(2).reciprocal(MathContext.UNLIMITED))
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testReciprocalUnlimitedFail() {
        BigDecimal.valueOf(3).reciprocal(MathContext.UNLIMITED)
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testReciprocalFail() {
        assertEquals(BigDecimal.valueOf(123), BigDecimal.ZERO.reciprocal(MC))
    }


    @Test
    fun testFactorialInt() {
        assertEquals(BigDecimal(1), factorial(0))
        assertEquals(BigDecimal(1), factorial(1))
        assertEquals(BigDecimal(2), factorial(2))
        assertEquals(BigDecimal(6), factorial(3))
        assertEquals(BigDecimal(24), factorial(4))
        assertEquals(BigDecimal(120), factorial(5))
        assertEquals(
            toBigDecimal("9425947759838359420851623124482936749562312794702543768327889353416977599316221476503087861591808346911623490003549599583369706302603264000000000000000000000000"),
            factorial(101)
        )
        var expected = BigDecimal.ONE
        for (n in 1..999) {
            expected = expected.multiply(BigDecimal.valueOf(n.toLong()))
            assertEquals(expected, factorial(n))
        }
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testFactorialIntNegative() {
        factorial(-1)
    }

    @Test
    fun testFactorial() {
        // Result from wolframalpha.com: 1.5!
        val expected =
            BigDecimal("1.3293403881791370204736256125058588870981620920917903461603558423896834634432741360312129925539084990621701")
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("1.5").factorial(mathContext) },
            10
        )
    }

    @Test
    fun testFactorialNegative() {
        // Result from wolframalpha.com: (-1.5)!
        val expected =
            BigDecimal("-3.544907701811032054596334966682290365595098912244774256427615579705822569182064362749901313477089330832453647248")
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("-1.5").factorial(mathContext) },
            10
        )
    }

    @Test
    fun testFactorialIntegerValues() {
        assertEquals(BigDecimal(1).round(MC), BigDecimal.valueOf(0).factorial(MC))
        assertEquals(BigDecimal(1).round(MC), BigDecimal.valueOf(1).factorial(MC))
        assertEquals(BigDecimal(2).round(MC), BigDecimal.valueOf(2).factorial(MC))
        assertEquals(BigDecimal(6).round(MC), BigDecimal.valueOf(3).factorial(MC))
        assertEquals(BigDecimal(24).round(MC), BigDecimal.valueOf(4).factorial(MC))
        assertEquals(BigDecimal(120).round(MC), BigDecimal.valueOf(5).factorial(MC))
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testFactorialNegative1() {
        BigDecimal.valueOf(-1).factorial(MC)
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testFactorialNegative2() {
        BigDecimal.valueOf(-2).factorial(MC)
    }

    @Test
    fun testGamma() {
        // Result from wolframalpha.com: gamma(1.5)
        val expected =
            BigDecimal("0.886226925452758013649083741670572591398774728061193564106903894926455642295516090687475328369272332708113411812")
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("1.5").gamma(mathContext) },
            10
        )
    }

    @Test
    fun testGammaSlightlyPositive() {
        // Result from wolframalpha.com: gamma(0.5)
        val expected =
            BigDecimal("1.77245385090551602729816748334114518279754945612238712821380778985291128459103218137495065673854466541622682362428")
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("0.5").gamma(mathContext) },
            10
        )
    }

    @Test
    fun testGammaNegative() {
        // Result from wolframalpha.com: gamma(-1.5)
        val expected =
            BigDecimal("2.36327180120735470306422331112152691039673260816318283761841038647054837945470957516660087565139288722163576483237676")
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("-1.5").gamma(mathContext) },
            10
        )
    }

    @Test
    fun testGammaIntegerValues() {
        assertEquals(BigDecimal(1).round(MC), BigDecimal.valueOf(1).gamma(MC))
        assertEquals(BigDecimal(1).round(MC), BigDecimal.valueOf(2).gamma(MC))
        assertEquals(BigDecimal(2).round(MC), BigDecimal.valueOf(3).gamma(MC))
        assertEquals(BigDecimal(6).round(MC), BigDecimal.valueOf(4).gamma(MC))
        assertEquals(BigDecimal(24).round(MC), BigDecimal.valueOf(5).gamma(MC))
        assertEquals(BigDecimal(120).round(MC), BigDecimal.valueOf(6).gamma(MC))
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testPowIntZeroPowerNegative() {
        BigDecimal.valueOf(0).pow(-5, MC)
    }

    @Test
    fun testPowIntPositiveY() {
        // positive exponents
        for (x in intArrayOf(-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5)) {
            for (y in intArrayOf(0, 1, 2, 3, 4, 5)) {
                assertEquals(
                    BigDecimal.valueOf(x.toDouble().pow(y.toDouble()).toLong()).round(MC),
                    BigDecimal.valueOf(x.toLong()).pow(y, MC),
                    "$x^$y",
                )
            }
        }
    }

    @Test
    fun testPowIntUnlimited() {
        assertEquals(BigDecimal.valueOf(1.44), BigDecimal.valueOf(1.2).pow(2, MathContext.UNLIMITED))
        assertEquals(BigDecimal.valueOf(0.25), BigDecimal.valueOf(2).pow(-2, MathContext.DECIMAL128))
    }

    @Test
    fun testPowIntUnnecessary() {
        val mathContext = MathContext(20, RoundingMode.UNNECESSARY)
        assertEquals(
            BigDecimal.valueOf(2.0736).round(mathContext),
            BigDecimal.valueOf(1.2).pow(4, mathContext)
        )
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testPowIntUnnecessaryFail() {
        BigDecimal.valueOf(1.2).pow(4, MathContext(3, RoundingMode.UNNECESSARY))
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testPowIntUnlimitedFail() {
        BigDecimal.valueOf(1.2).pow(-2, MathContext.UNLIMITED)
    }

    @Test
    fun testPowIntHighAccuracy() {
        // Result from wolframalpha.com: 1.000000000000001 ^ 1234567
        val expected = toBigDecimal(
            "1.0000000012345670007620772217746112884011264566574371750661936042203432730421791357400340579375261062151425984605455718643834831212687809215508627027381366482513893346638309647254328483125554030430209837119592796226273439855097892690164822394282109582106572606688508863981956571098445811521589634730079294115917257238821829137340388818182807197358582081813107978164190701238742379894183398009280170118101371420721038965387736053980576803168658232943601622524279972909569009054951992769572674935063940581972099846878996147233580891866876374475623810230198932136306920161303356757346458080393981632574418878114647836311205301451612892591304592483387202671500569971713254903439669992702668656944996771767101889159835990797016804271347502053715595561455746434955874842970156476866700704289785119355240166844949092583102028847019848438487206052262820785557574627974128427022802453099783875466674774383283633178630613523399806185908766812896743349684394795523513553454443796268439405430281375480024234032172402840564635266057234920659063946839453576870882295214918516855889289061559150620879201634277096704728220897344041618549870872138380388841708643468696894694958739051584506837702527545643699395947205334800543370866515060967536750156194684592206567524739086165295878406662557303580256110172236670067327095217480071365601062314705686844139397480994156197621687313833641789783258629317024951883457084359977886729599488232112988200551717307628303748345907910029990065217835915703110440740246602046742181454674636608252499671425052811702208797030657332754492225689850123854291480472732132658657813229027494239083970478001231283002517914471878332200542180147054941938310139493813524503325181756491235593304058711999763930240249546122995086505989026270701355781888675020326791938289147344430814703304780863155994800418441632244536"
        )
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("1.000000000000001").pow(1234567, mathContext) },
            10
        )
    }

    @Test
    fun testPowIntNegativeY() {
        // positive exponents
        for (x in intArrayOf(-5, -4, -3, -2, -1, 1, 2, 3, 4, 5)) { // no x=0 !
            for (y in intArrayOf(-5, -4, -3, -2, -1)) {
                assertEquals(
                    BigDecimal.ONE.divide(
                        BigDecimal.valueOf(
                            x.toDouble().pow(-y.toDouble()).toLong()
                        ), MC
                    ).round(MC),
                    BigDecimal.valueOf(x.toLong()).pow(y, MC),
                    "$x^$y",

                    )
            }
        }
    }


    @Test
    fun testPowIntSpecialCases() {
        // 0^0 = 1
        assertEquals(BigDecimal.valueOf(1).round(MC), BigDecimal.valueOf(0).pow(0, MC))
        // 0^x = 0 for x > 0
        assertEquals(BigDecimal.valueOf(0).round(MC), BigDecimal.valueOf(0).pow(+5, MC))

        // x^0 = 1 for all x
        assertEquals(BigDecimal.valueOf(1).round(MC), BigDecimal.valueOf(-5).pow(0, MC))
        assertEquals(BigDecimal.valueOf(1).round(MC), BigDecimal.valueOf(+5).pow(0, MC))
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testPowInt0NegativeY() {
        // 0^x for x < 0 is undefined
        println(BigDecimal.valueOf(0).pow(-5, MC))
    }


    @Test
    fun testPowPositiveX() {
        for (x in doubleArrayOf(1.0, 1.5, 2.0, 2.5, 3.0, 4.0, 5.0)) {
            for (y in doubleArrayOf(
                -5.0,
                -4.0,
                -3.0,
                -2.5,
                -2.0,
                -1.5,
                -1.0,
                -0.5,
                0.0,
                0.5,
                1.0,
                1.5,
                2.0,
                2.5,
                3.0,
                4.0,
                5.0
            )) {
                assertBigDecimal(
                    "$x^$y",
                    toCheck(x.pow(y))!!,
                    BigDecimal.valueOf(x).pow(BigDecimal.valueOf(y), MC),
                    MC_CHECK_DOUBLE
                )
            }
        }
        for (x in doubleArrayOf(0.0)) {
            for (y in doubleArrayOf(0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 4.0, 5.0)) {
                assertBigDecimal(
                    "$x^$y",
                    toCheck(x.pow(y))!!,
                    BigDecimal.valueOf(x).pow(BigDecimal.valueOf(y), MC),
                    MC_CHECK_DOUBLE
                )
            }
        }
    }

    @Test
    fun testPowNegativeX() {
        for (x in doubleArrayOf(-2.0, -1.0)) {
            for (y in doubleArrayOf(-5.0, -4.0, -3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0)) {
                assertBigDecimal(
                    "$x^$y",
                    toCheck(x.pow(y))!!,
                    BigDecimal.valueOf(x).pow(BigDecimal.valueOf(y), MC),
                    MC
                )
            }
        }
    }

    @Test
    fun testPowSpecialCases() {
        // 0^0 = 1
        assertEquals(
            BigDecimal.valueOf(1).round(MC),
            BigDecimal.valueOf(0).pow(BigDecimal.valueOf(0), MC)
        )
        // 0^x = 0 for x > 0
        assertEquals(
            BigDecimal.valueOf(0).round(MC),
            BigDecimal.valueOf(0).pow(BigDecimal.valueOf(+5), MC)
        )

        // x^0 = 1 for all x
        assertEquals(
            BigDecimal.valueOf(1).round(MC),
            BigDecimal.valueOf(-5).pow(BigDecimal.valueOf(0), MC)
        )
        assertEquals(
            BigDecimal.valueOf(1).round(MC),
            BigDecimal.valueOf(+5).pow(BigDecimal.valueOf(0), MC)
        )
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testPow0NegativeY() {
        // 0^x for x < 0 is undefined
        println(BigDecimal.valueOf(0).pow(BigDecimal.valueOf(-5), MC))
    }

    @Test
    fun testPowHighAccuracy1() {
        // Result from wolframalpha.com: 0.12345 ^ 0.54321
        val expected = toBigDecimal(
            "0.3209880595151945185125730942395290036641685516401211365668021036227236806558712414817507777010529315619538091221044550517779379562785777203521073317310721887789752732383195992338046561142233197839101366627988301068817528932856364705673996626318789438689474137773276533959617159796843289130492749319006030362443626367021658149242426847020379714749221060925227256780407031977051743109767225075035162749746755475404882675969237304723283707838724317900591364308593663647305926456586738661094577874745954912201392504732008960366344473904725152289010662196139662871362863747003357119301290791005303042638323919552042428899542474653695157843324029537490471818904797202183382709740019779991866183409872343305557416160635632389025962773383948534706993646814493361946320537133866646649868386696744314086907873844459873522561100570574729858449637845765912377361924716997579241434414109143219005616107946583880474580592369219885446517321145488945984700859989002667482906803702408431898991426975130215742273501237614632961770832706470833822137675136844301417148974010849402947454745491575337007331634736828408418815679059906104486027992986268232803807301917429934411578887225359031451001114134791114208050651494053415141140416237540583107162910153240598400275170478935634433997238593229553374738812677055332589568742194164880936765282391919077003882108791507606561409745897362292129423109172883116578263204383034775181118065757584408324046421493189442843977781400819942671602106042861790274528866034496106158048150133736995335643971391805690440083096190217018526827375909068556103532422317360304116327640562774302558829111893179295765516557567645385660500282213611503701490309520842280796017787286271212920387358249026225529459857528177686345102946488625734747525296711978764741913791309106485960272693462458335037929582834061232406160"
        )
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("0.12345").pow(BigDecimal("0.54321"), mathContext) },
            10
        )
    }

    @Test
    fun testPowHighAccuracy2() {
        // Result from wolframalpha.com: 1234.5 ^ 5.4321
        val expected = toBigDecimal(
            "62128200273178468.6677398330313037781753494560130835832101960387223758707669665725754895879107246310011029364211118269934534848627597104718365299707675269883473866053798863560099145230081124493870576780612499275723252481988188990085485417903685910250385975275407201318962063571641788853056193956632922391172489400257505790978314596080576631215805090936935676836971091464254857748180262699112027530753684170510323511798980747639116410705861310591624568227525136728034348718513230067867653958961909807085463366698897670703033966902227226026963721428348393842605660315775615215897171041744502317760375398468093874441545987214768846585209830041286071364933140664316884545264314137705612948991849327809564207354415319908754752255701802039139765434084951567836148382259822205056903343078315714330953561297888049627241752521508353126178543435267324563502039726903979264593590549404498146175495384414213014048644769191478319546475736458067346291095970042183567796890291583916374248166579807593334209446774446615766870268699990517113368293867016985423417705611330741518898131591089047503977721006889839010831321890964560951517989774344229913647667605138595803854678957098670003929907267918591145790413480904188741307063239101475728087298405926679231349800701106750462465201862628618772432920720630962325975002703818993580555861946571650399329644600854846155487513507946368829475408071100475344884929346742632438630083062705384305478596166582416332328006339035640924818942503261178020860473649223332292597947133883640686283632593820956826840942563265332271497540069352040396588314197259366049553760360493773149812879272759032356567261509967695159889106382819692093987902453799750689562469611095996225341555322139606462193260609916132372239906927497183040765412767764999503366952191218000245749101208123555266177028678838265168229"
        )
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("1234.5").pow(BigDecimal("5.4321"), mathContext) },
            10
        )
    }

    @Test
    fun testPowLargeInt() {
        val x = BigDecimal("1.5")
        val y = BigDecimal("1E10") // still possible with longValueExact()
        // Result from wolframalpha.com: 1.5 ^ 1E10
        val expected = BigDecimal("3.60422936590014149127041615892759056162677175765E1760912590")
        assertEquals(10000000000L, y.longValueExact())
        assertPrecisionCalculation(
            expected,
            { mathContext -> x.pow(y, mathContext) },
            20
        )
    }

    @Test
    fun testPowVeryLargeInt() {
        val x = BigDecimal("1.00000000000001")
        val y = BigDecimal("1E20") // not possible with than longValueExact()
        // Result from wolframalpha.com: 1.00000001 ^ 1E20
        val expected = BigDecimal("3.03321538163601059899125791999959844544825181205425E434294")
        assertPrecisionCalculation(
            expected,
            { mathContext -> x.pow(y, mathContext) },
            20
        )
    }

    @Test
    fun testPowRandom() {
        assertRandomCalculation(
            adaptCount(1000),
            "pow",
            { random -> random.nextDouble() * 100 + 0.000001 },
            { random -> random.nextDouble() * 100 - 50 }, { a: Double, b: Double -> a.pow(b) }
        ) { x, y, mathContext -> x.pow(y, mathContext) }
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testPowOverflow() {
        BigDecimal("123").pow(BigDecimal("1E20"), MC)
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testPowUnlimitedFail() {
        BigDecimal.valueOf(1.2).pow(BigDecimal.valueOf(1.1), MathContext.UNLIMITED)
    }

    @Test
    fun testSqrt() {
        for (value in doubleArrayOf(0.0, 0.1, 2.0, 4.0, 10.0, 16.0, 33.3333)) {
            assertBigDecimal(
                "sqrt($value)",
                toCheck(sqrt(value))!!,
                BigDecimal.valueOf(value).sqrt(MC),
                MC_CHECK_DOUBLE
            )
        }
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testSqrtUnlimitedFail() {
        BigDecimal.valueOf(2).sqrt(MathContext.UNLIMITED)
    }

    @Test
    fun testSqrtHighAccuracy() {
        // Result from wolframalpha.com: sqrt(2)
        val expected = toBigDecimal(
            "1.4142135623730950488016887242096980785696718753769480731766797379907324784621070388503875343276415727350138462309122970249248360558507372126441214970999358314132226659275055927557999505011527820605714701095599716059702745345968620147285174186408891986095523292304843087143214508397626036279952514079896872533965463318088296406206152583523950547457502877599617298355752203375318570113543746034084988471603868999706990048150305440277903164542478230684929369186215805784631115966687130130156185689872372352885092648612494977154218334204285686060146824720771435854874155657069677653720226485447015858801620758474922657226002085584466521458398893944370926591800311388246468157082630100594858704003186480342194897278290641045072636881313739855256117322040245091227700226941127573627280495738108967504018369868368450725799364729060762996941380475654823728997180326802474420629269124859052181004459842150591120249441341728531478105803603371077309182869314710171111683916581726889419758716582152128229518488472089694633862891562882765952635140542267653239694617511291602408715510135150455381287560052631468017127402653969470240300517495318862925631385188163478001569369176881852378684052287837629389214300655869568685964595155501644724509836896036887323114389415576651040883914292338113206052433629485317049915771756228549741438999188021762430965206564211827316726257539594717255934637238632261482742622208671155839599926521176252698917540988159348640083457085181472231814204070426509056532333398436457865796796519267292399875366617215982578860263363617827495994219403777753681426217738799194551397231274066898329989895386728822856378697749662519966583525776198939322845344735694794962952168891485492538904755828834526096524096542889394538646625744927556381964410316979833061852019379384940057156333720548068540575867999670121372239"
        )
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("2").sqrt(mathContext) },
            10
        )
    }

    @Test
    fun testSqrtHuge() {
        // Result from wolframalpha.com: sqrt(1e399)
        val expected = BigDecimal("3.1622776601683793319988935444327185337195551393252168E199")
        assertEquals(expected.round(MC), BigDecimal("1E399").sqrt(MC))
    }

    @Test
    fun testSqrtRandom() {
        assertRandomCalculation(
            adaptCount(1000),
            "sqrt",
            { random -> random.nextDouble() * 100 + 0.000001 }, { a: Double -> sqrt(a) }
        ) { x, mathContext -> x.sqrt(mathContext) }
    }

    /*
    @Test
    public void testSqrtJava9Random() {
        assertRandomCalculation(
                adaptCount(1000),
                "sqrt(x)",
                "java9 sqrt(x)",
                (random, mathContext) -> randomBigDecimal(random, mathContext),
                (x, mathContext) -> BigDecimalMath.sqrt(x, mathContext),
                (x, mathContext) -> x.sqrt(mathContext));
    }
    */

    /*
    @Test
    public void testSqrtJava9Random() {
        assertRandomCalculation(
                adaptCount(1000),
                "sqrt(x)",
                "java9 sqrt(x)",
                (random, mathContext) -> randomBigDecimal(random, mathContext),
                (x, mathContext) -> BigDecimalMath.sqrt(x, mathContext),
                (x, mathContext) -> x.sqrt(mathContext));
    }
    */

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testSqrtNegative() {
        BigDecimal(-1).sqrt(MC)
    }

    @Test
    fun testRootSqrtCbrt() {
        for (x in doubleArrayOf(0.0, 0.1, 1.0, 2.0, 10.0, 33.3333)) {
            assertBigDecimal(
                "root(2,$x)",
                toCheck(sqrt(x))!!,
                BigDecimal.valueOf(x).root(BigDecimal.valueOf(2), MC),
                MC_CHECK_DOUBLE
            )
            assertBigDecimal(
                "root(3,$x)",
                toCheck(Math.cbrt(x))!!,
                BigDecimal.valueOf(x).root(BigDecimal.valueOf(3), MC),
                MC_CHECK_DOUBLE
            )
        }
    }

    @Test
    fun testRoot() {
        for (n in doubleArrayOf(0.1, 0.9, 1.0, 1.1, 2.0, 10.0, 33.3333, 1234.5678)) {
            for (x in doubleArrayOf(0.0, 0.1, 0.9, 1.0, 1.1, 2.0, 10.0, 33.3333, 1234.5678)) {
                // println("ROOT x=$x n=$n")
                assertBigDecimal(
                    "root(2,$x)",
                    toCheck(x.pow(1.0 / n))!!,
                    BigDecimal.valueOf(x).root(BigDecimal.valueOf(n), MC),
                    MC_CHECK_DOUBLE
                )
            }
        }
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testRootZeroN() {
        BigDecimal.ONE.root(BigDecimal.ZERO, MC)
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testRootNegativeN() {
        BigDecimal.ONE.root(BigDecimal(-1), MC)
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testRootNegativeX() {
        BigDecimal(-1).root(BigDecimal.ONE, MC)
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testRootUnlimitedFail() {
        BigDecimal.valueOf(1.2).root(BigDecimal.valueOf(2), MathContext.UNLIMITED)
    }

    @Test
    fun testRootHighAccuracy1() {
        // Result from wolframalpha.com: root(1.23, 123)
        val expected = toBigDecimal(
            "50.016102539344819307741514415079435545110277821887074630242881493528776023690905378058352283823814945584087486290764920313665152884137840533937075179853255596515758851877960056849468879933122908090021571162427934915567330612627267701300492535817858361072169790783434196345863626810981153268939825893279523570322533446766188724600595265286542918045850353371520018451295635609248478721067200812355632099802713302132804777044107393832707173313768807959788098545050700242134577863569636367439867566923334792774940569273585734964008310245010584348384920574103306733020525390136397928777667088202296433541706175886006626333525007680397351405390927420825851036548474519239425298649420795296781692303253055152441850691276044546565109657012938963181532017974206315159305959543881191233733179735321461579808278383770345759408145745617032705494900390986476773247981270283533959979287340513398944113566999839889290733896874439682249327621463735375868408190435590094166575473967368412983975580104741004390308453023021214626015068027388545767003666342291064051883531202983476423138817666738346033272948508395214246047027012105246939488877506475824651688812245962816086719050192476878886543996441778751825677213412487177484703116405390741627076678284295993334231429145515176165808842776515287299275536932744066126348489439143701880784521312311735178716650919024092723485314329094064704170548551468318250179561508293077056611877488417962195965319219352314664764649802231780262169742484818333055713291103286608643184332535729978330383356321740509817475633105247757622805298711765784874873240679024286215940395303989612556865748135450980540945799394622053158729350598632915060818702520420240989908678141379300904169936776618861221839938283876222332124814830207073816864076428273177778788053613345444299361357958409716099682468768353446625063"
        )
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal.valueOf(123).root(BigDecimal.valueOf(1.23), mathContext) },
            10
        )
    }

    @Test
    fun testRootHighAccuracy2() {
        // Result from wolframalpha.com: root(7.5, 123)
        val expected = toBigDecimal(
            "1.8995643695815870676539369434054361726808105217886880103090875996194822396396255621113000403452538887419132729641364085738725440707944858433996644867599831080192362123855812595483776922496542428049642916664676504355648001147425299497362249152998433619265150901899608932149147324281944326398659053901429881376755786331063699786297852504541315337453993167176639520666006383001509553952974478682921524643975384790223822148525159295285828652242201443762216662072731709846657895992750535254286493842754491094463672629441270037173501058364079340866564365554529160216015597086145980711187711119750807640654996392084846441696711420521658760165363535215241687408369549643269709297427044177507157609035697648282875422321141920576120188389383509318979064825824777240151847818551071255436323480281154877997743553609520167536258202911691329853232693386770937694807506144279660147324316659333074620896627829029651910783066736606497262785345465872401993026696735802446138584306213230373571409591420951964537136053258998945471633936332983896917810023265095766395377592848121611444196796785031727740335105553348270077424620974061727975050161324060753928284759055040064976732991126510635738927993365006832681484889202649313814280125684525505938973967575274196130269615461251746873419445856759329916403947432038902141704646304799083820073914767560878449162496519826664715572693747490088659968040153989493366037393989012508491856761986732685422561958101646754270192269505879594808800416777471196270722586367363680538183391904535845392721112874375802640395545739073303112631715831096156004422381940090623765493332249827278090443678800852264922795299927727708248191560574252923342860845325222035245426918719153132138325983001330317244830727602810422542012322698940744820925849667642343510406965273569391887099540050259962759858771196756422007171"
        )
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal.valueOf(123).root(BigDecimal.valueOf(7.5), mathContext) },
            10
        )
    }

    @Test
    fun testRootRandom() {
        assertRandomCalculation(
            adaptCount(100),
            "root",
            { random -> random.nextDouble() * 10 + 0.000001 },
            { random -> random.nextDouble() * 5 },
            null,
            { x, y, mathContext -> x.root(y, mathContext) })
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testRootNegative() {
        BigDecimal(-1).root(BigDecimal.ONE, MC)
    }

    @Test
    fun testLogRange10() {
        val step = BigDecimal.valueOf(getRangeStep(0.1))

        var x = step
        while (x < BigDecimal.TEN) {
            // println("Testing log($x)")
            assertBigDecimal(
                "log($x)",
                toCheck(ln(x.toDouble()))!!,
                x.log(MC),
                MC_CHECK_DOUBLE
            )
            val finalX: BigDecimal = x
            assertPrecisionCalculation(
                { mathContext -> finalX.log(mathContext) },
                10, AUTO_TEST_MAX_PRECISION
            )
            x += step
        }


        x = step
        while (x < BigDecimal.TEN) {
            // println("Testing log($x)")
            assertBigDecimal(
                "log($x)",
                toCheck(ln(x.toDouble()))!!,
                x.log(MC),
                MC_CHECK_DOUBLE
            )
            val finalX: BigDecimal = x
            assertPrecisionCalculation(
                { mathContext -> finalX.log(mathContext) },
                10, AUTO_TEST_MAX_PRECISION
            )
            x += step
        }
    }

    @Test
    fun testLogRange100() {
        val step = BigDecimal.valueOf(getRangeStep(1.0))

        var x = step
        while (x < BigDecimal.TEN) {
            assertBigDecimal(
                "log($x)",
                toCheck(ln(x.toDouble()))!!,
                x.log(MC),
                MC_CHECK_DOUBLE
            )
            val finalX: BigDecimal = x
            assertPrecisionCalculation(
                { mathContext -> finalX.log(mathContext) },
                10, AUTO_TEST_MAX_PRECISION
            )
            x += step
        }
    }

    @Test
    fun testLogHighAccuracy1() {
        // Result from wolframalpha.com: ln(0.1)
        val expected = toBigDecimal(
            "-2.30258509299404568401799145468436420760110148862877297603332790096757260967735248023599720508959829834196778404228624863340952546508280675666628736909878168948290720832555468084379989482623319852839350530896537773262884616336622228769821988674654366747440424327436515504893431493939147961940440022210510171417480036880840126470806855677432162283552201148046637156591213734507478569476834636167921018064450706480002775026849167465505868569356734206705811364292245544057589257242082413146956890167589402567763113569192920333765871416602301057030896345720754403708474699401682692828084811842893148485249486448719278096762712757753970276686059524967166741834857044225071979650047149510504922147765676369386629769795221107182645497347726624257094293225827985025855097852653832076067263171643095059950878075237103331011978575473315414218084275438635917781170543098274823850456480190956102992918243182375253577097505395651876975103749708886921802051893395072385392051446341972652872869651108625714921988499787488737713456862091670584980782805975119385444500997813114691593466624107184669231010759843831919129223079250374729865092900988039194170265441681633572755570315159611356484654619089704281976336583698371632898217440736600916217785054177927636773114504178213766011101073104239783252189489881759792179866639431952393685591644711824675324563091252877833096360426298215304087456092776072664135478757661626292656829870495795491395491804920906943858079003276301794150311786686209240853794986126493347935487173745167580953708828106745244010589244497647968607512027572418187498939597164310551884819528833074669931781463493000032120032776565413047262188397059679445794346834321839530441484480370130575367426215367557981477045803141363779323629156012818533649846694226146520645994207291711937060244492"
        )
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("0.1").log(mathContext) },
            10
        )
    }

    @Test
    fun testLogHighAccuracy2() {
        // Result from wolframalpha.com: ln(1.1)
        val expected = toBigDecimal(
            "0.0953101798043248600439521232807650922206053653086441991852398081630010142358842328390575029130364930727479418458517498888460436935129806386890150217023263755687346983551204157456607731117050481406611584967219092627683199972666804124629171163211396201386277872575289851216418802049468841988934550053918259553296705084248072320206243393647990631942365020716424972582488628309770740635849277971589257686851592941134955982468458204470563781108676951416362518738052421687452698243540081779470585025890580291528650263570516836272082869034439007178525831485094480503205465208833580782304569935437696233763597527612962802332419887793490159262767738202097437296124304231269978317763387834500850947983607954894765663306829441000443449252110585597386446423305000249520642003351749383035733163887183863658864095987980592896922224719866617664086469438599082172014984648661016553883267832731905893594398418365160836037053676940083743785539126726302367554039807719021730407981203469520199824994506211545156995496539456365581027383589659382402015390419603824664083368873307873019384357785045824504691072378535575392646883979065139246126662251603763318447377681731632334250380687464278805888614468777887659631017437620270326399552535490068490417697909725326896790239468286121676873104226385183016443903673794887669845552057786043820598162664741719835262749471347084606772426040314789592161567246837020619602671610506695926435445325463039957620861253293473952704732964930764736250291219054949541518603372096218858644670199237818738241646938837142992083372427353696766016209216197009652464144415416340684821107427035544058078681627922043963452271529803892396332155037590445683916173953295983049207965617834301297873495901595044766960173144576650851894013006899406665176310040752323677741807454239794575425116685728529323731335086049670268306"
        )
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("1.1").log(mathContext) },
            10
        )
    }

    @Test
    fun testLogHighAccuracy3() {
        // Result from wolframalpha.com: ln(12345.6)
        val expected = toBigDecimal(
            "9.42105500327135525114521501453525399237436111276300326386323534432727151942992511520562558738175320737219392933678106934681377640298797158131139323906361961480381516008415766949640011144295651380957422777114172279167654006534622812747920122075143832000303491928864417567534602811492095685408856581074035803357797631847469251006466446952382984400769172787795491275890878474305023861509824367243299385769279771744041937866552134975148449991501344008449686333627176197439283560717007769286520651804657135365525410547797134491863813264296599988480767570621877413992243488449252058389112464675521921368744908030643106093708139694498213865760209374231089223703469389057990578641477811580679006647361045368883126313166757159295044784734054746026667561208850147352459931288221690064827656007945926558137817955314752299200021125335319543610643148781413031739368946686197126231424703883123190210238015791369611214420726133482521541649129324232190740641049135517129893844376556993789191631768552752257796461172834352906322971133196717292014063557464657868471260257837864581817895933554699436597231519928906186824100551929174973211768975723220457184410041128885431823059460270296159512608527194960997843854276107619358871611335110158160499192067423059751567986373407423489599586293284362977309927604782683386482396609096117347165767675657470578510018397575185923185572052807175571518796143517238193372303027925460053807069802388627060672427272087223286476333683468229892546440731981947511457788744089944064466689422654892614083398427300212135529866471079161390374604296893598724751037581346990096479637907462110313260901383748633868418336284029147686046156013978973990920093756659785588328734878986910751799701679853456356654554727303139653731884939067754728654663370026652097310980166441905496504187282659704649813546716585697691"
        )
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("12345.6").log(mathContext) },
            10
        )
    }

    @Test
    fun testLogNotorious1() {
        // Result from wolframalpha.com: ln(3.627)
        // result contains digit sequence 249999790 which is tricky
        val expected =
            BigDecimal("1.2884058603007653127191678758996346017468835249999790635451685475107168413271219046526793891352454018922718349868595520309101297629519193857088741003717779597615671244988769478607734963041591967679662838292064119163503909719852563871678824241371281215403569416162383989623852") // 6801424419472197899141291552341986057193552767761847325665588799624460996389716450246797586099819246857022106263044473561032621692801928684892761931286774706996443604259279886700716")
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("3.627").log(mathContext) },
            10
        )
    }

    @Test
    fun testLogSmall() {
        // Result from wolframalpha.com: log(1e-399)
        val expected = BigDecimal("-918.731452104624227923178590419061318832839493962880417437")
        assertEquals(expected.round(MC), BigDecimal("1E-399").log(MC))
    }

    @Test
    fun testLogHuge() {
        // Result from wolframalpha.com: log(1e399)
        val expected = BigDecimal("918.7314521046242279231785904190613188328394939628804174372")
        assertEquals(expected.round(MC), BigDecimal("1E399").log(MC))
    }

    @Test
    fun testLog10WithPositivePowersOfTen() {
        val mathContext = MathContext(50)
        var x = BigDecimal("1")
        var expectedLog10 = BigDecimal(0)
        for (i in 0..19) {
            val actualLog10: BigDecimal = x.log10(mathContext)
            assertEquals(true, expectedLog10.compareTo(actualLog10) == 0)
            x = x.multiply(BigDecimal.TEN, mathContext)
            expectedLog10 = expectedLog10.add(BigDecimal.ONE, mathContext)
        }
    }

    @Test
    fun testLog10WithNegativePowersOfTen() {
        val mathContext = MathContext(50)
        var x = BigDecimal("1")
        var expectedLog10 = BigDecimal(0)
        for (i in 0..19) {
            val actualLog10: BigDecimal = x.log10(mathContext)
            assertEquals(true, expectedLog10.compareTo(actualLog10) == 0)
            x = x.divide(BigDecimal.TEN, mathContext)
            expectedLog10 = expectedLog10.subtract(BigDecimal.ONE, mathContext)
        }
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testLog10UnlimitedFail() {
        BigDecimal.valueOf(2).log10(MathContext.UNLIMITED)
    }

    @Test
    fun testLogRandom() {
        assertRandomCalculation(
            adaptCount(100),
            "log",
            { random -> random.nextDouble() * 100 + 0.00001 }, { a: Double -> ln(a) }
        ) { x, mathContext -> x.log(mathContext) }
    }

    @Test
    fun testLog2Random() {
        assertRandomCalculation(
            adaptCount(100),
            "log",
            { random -> random.nextDouble() * 100 + 0.00001 },
            { x -> ln(x) / ln(2.0) }
        ) { x, mathContext -> x.log2(mathContext) }
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testLog2UnlimitedFail() {
        BigDecimal.valueOf(2).log2(MathContext.UNLIMITED)
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testLogNegative() {
        BigDecimal.valueOf(-1).log(MC)
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testLogUnlimitedFail() {
        BigDecimal.valueOf(2).log(MathContext.UNLIMITED)
    }

    @Test
    fun testExp() {
        for (value in doubleArrayOf(-5.0, -1.0, 0.1, 2.0, 10.0)) {
            assertBigDecimal(
                "exp($value)",
                toCheck(exp(value))!!,
                BigDecimal.valueOf(value).exp(MC),
                MC_CHECK_DOUBLE
            )
        }
    }

    @Test
    fun testExpHuge() {
        // Largest exp(10^x) that still gives a result on Wolfram Alpha
        // exp(1000000000) = 8.00298177066097253304190937436500068878231499717... * 10^434294481
        val expected = BigDecimal("8.00298177066E434294481")
        val actual: BigDecimal = BigDecimal.valueOf(1000000000).exp(MathContext(12))
        assertEquals(expected, actual)
    }

    @Test
    fun testExp1E() {
        var precision = 1
        while (precision <= 2001) {
            val mathContext = MathContext(precision)
            assertEquals(
                toCheck(e(mathContext)),
                toCheck(BigDecimal.ONE.exp(mathContext)),
                "exp(1)"
            )
            precision += 100
        }
    }

    @Test
    fun testExpHighAccuracy1() {
        // Result from wolframalpha.com: exp(0.1)
        val expected = toBigDecimal(
            "1.1051709180756476248117078264902466682245471947375187187928632894409679667476543029891433189707486536329171204854012445361537347145315787020068902997574505197515004866018321613310249357028047934586850494525645057122112661163770326284627042965573236001851138977093600284769443372730658853053002811154007820888910705403712481387499832879763074670691187054786420033729321209162792986139109713136202181843612999064371057442214441509033603625128922139492683515203569550353743656144372757405378395318324008280741587539066613515113982139135726893022699091000215648706791206777090283207508625041582515035160384730085864811589785637025471895631826720701700554046867490844416060621933317666818019314469778173494549497985045303406629427511807573756398858555866448811811806333247210364950515781422279735945226411105718464916466588898895425154437563356326922423993425668055030150187978568089290481077628854935380963680803086975643392286380110893491216896970405186147072881173903395370306903756052863966751655566156177044091023716763999613715961429909147602055822171056918247483370329310652377494326018131931115202583455695740577117305727325929270892586003078380276849851024733440526333630939768046873818746897979176031710638428538365444373036344477660068827517905394205724765809719068497652979331103372768988364106139063845834332444587680278142035133567220351279735997089196132184270510670193246409032174006524564495804123904224547124821906736781803247534842994079537510834190198353331683651574603364551464993636940684957076677363104098202444018343049556576017452467191522001230198866508508728780804296630956390659819928014152407848066718063601429519635764058390569704470217925967541099757148635387989599481795155282833193600584112822014656645896726556449347326910544815360769564296952628696236865028848565540573895707695598984577773238"
        )
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("0.1").exp(mathContext) },
            10
        )
    }

    @Test
    fun testExpHighAccuracy2() {
        // Result from wolframalpha.com: exp(123.4)
        val expected = toBigDecimal(
            "390786063200889154948155379410925404241701821048363382.932350954191939407875540538095053725850542917235991826991631549658381619846119064767940229694652504799690942074237719293556052198585602941442651814977379463173507703540164446248233994372649675083170661574855926134163163649067886904058135980414181563116455815478263535970747684634869846370078756117785925810367190913580101129012440848783613501818345221727921636036313301394206941005430607708535856550269918711349535144151578877168501672228271098301349906118292542981905746359989070403424693487891904874342086983801039403550584241691952868285098443443717286891245248589074794703961309335661643261592184482383775612097087066220605742406426487994296854782150419816494210555905079335674950579368212272414401633950719948812364415716009625682245889528799300726848267101515893741151833582331196187096157250772884710690932741239776061706938673734755604112474396879294621933875319320351790004373826158307559047749814106033538586272336832756712454484917457975827690460377128324949811226379546825509424852035625713328557508831082726245169380827972015037825516930075858966627188583168270036404466677106038985975116257215788600328710224325128935656214272530307376436037654248341541925033083450953659434992320670198187236379508778799056681080864264023524043718014105080505710276107680142887912693096434707224622405921182458722451547247803222237498053677146957211048297712875730899381841541047254172958887430559055751735318481711132216206915942752379320012433097749980476094039036829992786175018479140791048841069099146681433638254527364565199203683980587269493176948487694117499339581660653106481583097500412636413209554147009042448657752082659511080673300924304690964484273924800648584285968546527296722686071417123776801220060226116144242129928933422759721847194902947144831258"
        )
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("123.4").exp(mathContext) },
            60
        )
    }

    @Test
    fun testExpRandom() {
        assertRandomCalculation(
            adaptCount(1000),
            "exp",
            { random -> random.nextDouble() * 100 - 50 }, { a: Double -> exp(a) }
        ) { x, mathContext -> x.exp(mathContext) }
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testExpUnlimitedFail() {
        BigDecimal.valueOf(2).exp(MathContext.UNLIMITED)
    }

    @Test
    fun testSin() {
        for (value in doubleArrayOf(-10.0, -5.0, -1.0, -0.3, 0.0, 0.1, 2.0, 10.0, 20.0, 222.0)) {
            assertBigDecimal(
                "sin($value)",
                toCheck(sin(value))!!,
                BigDecimal.valueOf(value).sin(MC),
                MC_CHECK_DOUBLE
            )
        }
    }

    @Test
    fun testSinHighAccuracy() {
        // Result from wolframalpha.com: sin(1.234)
        val expected = toBigDecimal(
            "0.9438182093746337048617510061568275895172142720765760747220911781871399906968099483012598865055627858443507995518738766093869470509555068501582327052306784505752678705592908705201008148688700290760777223780263846758767378849305659165171458418076473553139600704400668632728702595059340199442411041490960324146869032516728992265808389968786198384238945833333329583982909393226122072922972072082343881982280834707504367506003311264818344731205557095928837491316071651630909050078777342482603092413467227932481298625668189293277970973821823536368859836352290171029827678389361668326651223313262181049179177713541062354198699357532113523026870736528786100665809233401695953717292150408826019906221690064294418649612406003915087946369501457359604343584263199153607653049282756925573849745513783165941970858623580447565222079996405576286670288022685431434886874295950242554364666123772837748084818582410730641892357161908769689946576427006541439717287833624991188137124554987468952436155712514180011917087180464841510692660163853984256220178122573051503993728719511214066957647751102014250171535662112264708511179562539851056691807479887430154563476132015884380272176766265870281843666030351481875369524292556759059067229573601315888931475939650530190997869732280644783380897437687282157862590038715019700476516674872568434184233136320198348795549241647388226943616471234865808472025746819601113742677172125085499919170003010129528504832452877371569832101275092363746925641703736428733071588960741542241552270894271703880793621738884941850045978201484407786879714905305225922874339567944723190660416232538921600185338494145628390029969393239498992087392435528382285271962107670662847438424222622822172719234821254495443425396088216409484488852445333426778397941937246299790022429378799080231482904310254381416336471042617299708975"
        )
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("1.234").sin(mathContext) },
            10
        )
    }

    @Test
    fun testSinRandom() {
        testSinRandom(100)
    }

    private fun testSinRandom(count: Int) {
        assertRandomCalculation(
            adaptCount(count),
            "sin",
            { random -> random.nextDouble() * 100 - 50 }, { a: Double -> sin(a) }
        ) { x, mathContext -> x.sin(mathContext) }
    }

    private fun runMultiThreaded(fn: () -> Unit): Unit {
        for (i in 1..30) {
            thread {
                try {
                    fn()
                } catch (e: Exception) {
                    println(e.localizedMessage)
                }
            }
        }
    }

    @Test
    fun testSinRandomMultiThreaded() {
        runMultiThreaded { testSinRandom(10) }
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testSinUnlimitedFail() {
        BigDecimal.valueOf(2).sin(MathContext.UNLIMITED)
    }

    @Test
    fun testAsin() {
        for (value in doubleArrayOf(-1.0, -0.999, -0.9, -0.1, 0.0, 0.1, 0.9, 0.999, 1.0)) {
            assertBigDecimal(
                "asin($value)",
                toCheck(asin(value))!!,
                BigDecimal.valueOf(value).asin(MC),
                MC_CHECK_DOUBLE
            )
        }
    }

    @Test
    fun testAsinRandom() {
        testAsinRandom(100)
    }

    private fun testAsinRandom(count: Int) {
        assertRandomCalculation(
            adaptCount(count),
            "asin",
            { random -> random.nextDouble() * 2 - 1 }, { a: Double -> asin(a) }
        ) { x, mathContext -> x.asin(mathContext) }
    }

    @Test
    // @Throws(Throwable::class)
    fun testAsinRandomMultiThreaded() {
        runMultiThreaded { testAsinRandom(10) }
    }


    @Test(expectedExceptions = [ArithmeticException::class])
    fun testAsinGreaterOne() {
        BigDecimal("1.00001").asin(MC)
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testAsinSmallerMinusOne() {
        BigDecimal("-1.00001").asin(MC)
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testAsinUnlimitedFail() {
        BigDecimal.valueOf(2).asin(MathContext.UNLIMITED)
    }

    @Test
    fun testCos() {
        for (value in doubleArrayOf(-5.0, -1.0, -0.3, 0.0, 0.1, 2.0, 10.0)) {
            assertBigDecimal(
                "cos($value)",
                toCheck(cos(value))!!,
                BigDecimal.valueOf(value).cos(MC),
                MC_CHECK_DOUBLE
            )
        }
    }

    @Test
    fun testCosHighAccuracy() {
        // Result from wolframalpha.com: cos(1.234)
        val expected = toBigDecimal(
            "0.3304651080717298574032807727899271437216920101969540348605304415152510377850481654650247150200780863415535299490857568279769354541967379397431278152484662377393883768410533419456683721348368071060447119629226464127475191769149818089642412919990646712138828462407239787011203786844401859479654861215480468553428321928822608813865008312100125205763217809424012405019490461648738007730900576327363563072819683608077467442286094847912950576189413624713414163958384339772584148744200648200688260933678578647517949013249027860144759454924413798901254668352778102301380649346953594529136819938821616590614874123930824463095104424946532966863750206459438812141713997562660701774968530149079881716322567945593156313333714539747617833144412172753445042952390635799639722239182963246046253903297563427741240081854182759746064810195237864060495745282046388159544259160022883886283097655348787382625328541498058884531961700370121969709480517496749271767735566816249479148488140162802977360971480510530896749944967304972380342831111213248738743617588927820627474733980422901948506009170945896565358929343777077336070289567245971065005860921723126096986632224093068775586235017140132374230378564807873973345322857782900999655081761884197357196908109838154083921138904571471346009606070648486103795109388774364448499820533743041120697352743044140279966823607345221684081898024173036376672034911709557102798619864101440725109041264516550229345850413762376113868869256025801898710854538411051622029568572639882301754336762028948110406127835411158515890274188501674397646117070538768699719967119559314804437052735458481025364866752041137855637961697664203246176781407193905595472755222134533679020285886126388322265972029035063590381025908006103799793443322205833892605275969980406879438015951448721792889383476504454337544038606643477976186"
        )
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("1.234").cos(mathContext) },
            10
        )
    }

    @Test
    fun testCosRandom() {
        testCosRandom(100)
    }

    private fun testCosRandom(count: Int) {
        assertRandomCalculation(
            adaptCount(count),
            "cos",
            { random -> random.nextDouble() * 100 - 50 }, { a: Double -> cos(a) }
        ) { x, mathContext -> x.cos(mathContext) }
    }

    @Test
    @Throws(Throwable::class)
    fun testCosRandomMultiThreaded() {
        runMultiThreaded { testCosRandom(10) }
    }


    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testCosUnlimitedFail() {
        BigDecimal.valueOf(2).cos(MathContext.UNLIMITED)
    }

    @Test
    fun testAcosRandom() {
        testAcosRandom(100)
    }

    private fun testAcosRandom(count: Int) {
        assertRandomCalculation(
            adaptCount(count),
            "acos",
            { random -> random.nextDouble() * 2 - 1 }, { a: Double -> acos(a) }
        ) { x, mathContext -> x.acos(mathContext) }
    }

    @Test
    @Throws(Throwable::class)
    fun testAcosRandomMultiThreaded() {
        runMultiThreaded { testAcosRandom(10) }
    }

    @Test
    fun testAcosMinusOne() {
        var precision = 1
        while (precision <= 2001) {
            val mathContext = MathContext(precision)
            val pi: BigDecimal = pi(mathContext)
            val acosMinusOne: BigDecimal = (-BigDecimal.ONE).acos(mathContext)
            assertEquals(true, pi.compareTo(acosMinusOne) == 0)
            precision += 100
        }
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testAcosGreaterOne() {
        BigDecimal("1.00001").acos(MC)
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testAcosSmallerMinusOne() {
        BigDecimal("-1.00001").acos(MC)
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testAcosUnlimitedFail() {
        BigDecimal.valueOf(2).acos(MathContext.UNLIMITED)
    }

    @Test
    fun testTan() {
        for (value in doubleArrayOf(1.1, -10.0, -5.0, -1.0, -0.3, 0.0, 0.1, 2.0, 10.0, 20.0, 222.0)) {
            assertBigDecimal(
                "tan($value)",
                toCheck(tan(value))!!,
                BigDecimal.valueOf(value).tan(MC),
                MC_CHECK_DOUBLE
            )
        }
    }

    @Test
    fun testTanRandom() {
        testTanRandom(100)
    }

    private fun testTanRandom(count: Int) {
        assertRandomCalculation(
            adaptCount(count),
            "tan",
            { random -> random.nextDouble() * 100 - 50 }, { a: Double -> tan(a) }
        ) { x, mathContext -> x.tan(mathContext) }
    }

    @Test
    @Throws(Throwable::class)
    fun testTanRandomMultiThreaded() {
        runMultiThreaded { testTanRandom(10) }
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testTanUnlimitedFail() {
        BigDecimal.valueOf(2).tan(MathContext.UNLIMITED)
    }

    @Test
    fun testAtanRandom() {
        testAtanRandom(100)
    }

    private fun testAtanRandom(count: Int) {
        assertRandomCalculation(
            adaptCount(count),
            "atan",
            { random -> random.nextDouble() * 100 - 50 }, { a: Double -> atan(a) }
        ) { x, mathContext -> x.atan(mathContext) }
    }

    @Test
    @Throws(Throwable::class)
    fun testAtanRandomMultiThreaded() {
        runMultiThreaded { testAtanRandom(10) }
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testAtan2ZeroZero() {
        BigDecimal.ZERO.atan2(BigDecimal.ZERO, MC)
    }

    @Test
    fun testAtan2() {
        val pi: BigDecimal = pi(MC)
        val piHalf: BigDecimal = pi(MathContext(MC.precision + 10)).divide(BigDecimal.valueOf(2), MC)
        assertEquals(piHalf, BigDecimal.TEN.atan2(BigDecimal.ZERO, MC))
        assertEquals(piHalf.negate(), BigDecimal.TEN.negate().atan2(BigDecimal.ZERO, MC))
        assertEquals(pi, BigDecimal.ZERO.atan2(BigDecimal.TEN.negate(), MC))
    }

    @Test
    fun testAtan2HighAccuracy() {
        // Result from wolframalpha.com: atan2(123456789, 987654321)
        val expected =
            toBigDecimal("0.12435499342522297334968147683476071896899844881294839643180323485370657121551589550118807775010954424614161504017100766102274760347773144234717264025098792427062054083201522193127589446759654134052154287720616792464947608894974472784377275294647356650781819500878799427824049369273085965502705708393722924586235499530689424922197361614748436463923316792256542944548723768578591781632376382102987565485177288344132765819127765508975729310469873258293578995")
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("123456789").atan2(BigDecimal("987654321"), mathContext) },
            10
        )
    }

    @Test
    fun testAtan2Random() {
        testAtan2Random(100)
    }

    private fun testAtan2Random(count: Int) {
        assertRandomCalculation(
            adaptCount(count),
            "atan2",
            { random -> random.nextDouble() * 100 - 50 },
            { random -> random.nextDouble() * 100 - 50 },
            { y: Double, x: Double -> atan2(y, x) }
        ) { y, x, mathContext ->
            val result: BigDecimal = y.atan2(x, mathContext)
            val pi: BigDecimal = pi(mathContext)
            if (result < pi.negate() || result > pi) {
                fail("outside allowed range: $result for y=$y, x=$x")
            }
            result
        }
    }

    @Test
    @Throws(Throwable::class)
    fun testAtan2RandomMultiThreaded() {
        runMultiThreaded { testAtan2Random(10) }
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testAtanUnlimitedFail() {
        BigDecimal.valueOf(2).atan(MathContext.UNLIMITED)
    }

    @Test
    fun testCot() {
        for (value in doubleArrayOf(0.5, -0.5)) {
            assertBigDecimal(
                "cot($value)",
                toCheck(cot(value))!!,
                BigDecimal.valueOf(value).cot(MC),
                MC_CHECK_DOUBLE
            )
        }
    }

    private fun cot(x: Double): Double {
        return cos(x) / sin(x)
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testCotUnlimitedFail() {
        BigDecimal.valueOf(2).cot(MathContext.UNLIMITED)
    }


    @Test
    fun testSinhRandom() {
        testSinhRandom(100)
    }

    private fun testSinhRandom(count: Int) {
        assertRandomCalculation(
            adaptCount(count),
            "sinh",
            { random -> random.nextDouble() * 100 - 50 }, { x: Double -> sinh(x) }
        ) { x, mathContext -> x.sinh(mathContext) }
    }

    @Test
    @Throws(Throwable::class)
    fun testSinhRandomMultiThreaded() {
        runMultiThreaded { testSinhRandom(10) }
    }

    @Test
    fun testAsinhRandom() {
        testAsinhRandom(100)
    }

    private fun testAsinhRandom(count: Int) {
        assertRandomCalculation(
            adaptCount(count),
            "asinh",
            { random -> random.nextDouble() * 100 - 50 },
            { v: Double -> asinh(v) }
        ) { x, mathContext -> x.asinh(mathContext) }
    }

    @Test
    @Throws(Throwable::class)
    fun testAsinhRandomMultiThreaded() {
        runMultiThreaded { testAsinhRandom(10) }
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testAsinhUnlimitedFail() {
        BigDecimal.valueOf(2).asinh(MathContext.UNLIMITED)
    }

    private fun asinh(x: Double): Double {
        return ln(x + sqrt(x * x + 1))
    }

    @Test
    fun testAcoshRandom() {
        testAcoshRandom(100)
    }

    private fun testAcoshRandom(count: Int) {
        assertRandomCalculation(
            adaptCount(count),
            "acosh",
            { random -> random.nextDouble() * 100 + 1 },
            { v: Double -> acosh(v) }
        ) { x, mathContext -> x.acosh(mathContext) }
    }

    @Test
    @Throws(Throwable::class)
    fun testAcoshRandomMultiThreaded() {
        runMultiThreaded { testAcoshRandom(10) }
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testAcoshUnlimitedFail() {
        BigDecimal.valueOf(2).acosh(MathContext.UNLIMITED)
    }

    private fun acosh(x: Double): Double {
        return ln(x + sqrt(x * x - 1))
    }

    @Test
    fun testAtanhRandom() {
        testAtanhRandom(100)
    }

    private fun testAtanhRandom(count: Int) {
        assertRandomCalculation(
            adaptCount(count),
            "atanh",
            { random -> random.nextDouble() * 1.9999 - 1 },
            { v: Double -> atanh(v) }
        ) { x, mathContext -> x.atanh(mathContext) }
    }

    @Test
    fun testAtanhRandomMultiThreaded() {
        runMultiThreaded { testAtanhRandom(10) }
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testAtanhFailOne() {
        BigDecimal.ONE.atanh(MC)
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testAtanhFailMinusOne() {
        BigDecimal.ONE.negate().atanh(MC)
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testAtanhUnlimitedFail() {
        BigDecimal.valueOf(0).atanh(MathContext.UNLIMITED)
    }

    private fun atanh(x: Double): Double {
        return ln((1 + x) / (1 - x)) / 2
    }

    @Test
    fun testAcothRandom() {
        testAcothRandom(100)
    }

    private fun testAcothRandom(count: Int) {
        assertRandomCalculation(
            adaptCount(count),
            "acoth",
            { random -> random.nextDouble() * 100 + 1 },
            { v: Double -> acoth(v) }
        ) { x, mathContext -> x.acoth(mathContext) }
        assertRandomCalculation(
            adaptCount(count),
            "acoth",
            { random -> -(random.nextDouble() * 100 + 1) },
            { v: Double -> acoth(v) }
        ) { x, mathContext -> x.acoth(mathContext) }
    }

    @Test
    fun testAcothRandomMultiThreaded() {
        runMultiThreaded { testAcothRandom(10) }
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testAcothUnlimitedFail() {
        BigDecimal.valueOf(2).acoth(MathContext.UNLIMITED)
    }

    private fun acoth(x: Double): Double {
        return ln((x + 1) / (x - 1)) / 2
    }

    @Test
    fun testCoshRandom() {
        testCoshRandom(1000)
    }

    private fun testCoshRandom(count: Int) {
        assertRandomCalculation(
            adaptCount(count),
            "cosh",
            { random -> random.nextDouble() * 100 - 50 }, { x: Double -> cosh(x) }
        ) { x, mathContext -> x.cosh(mathContext) }
    }

    @Test
    @Throws(Throwable::class)
    fun testCoshRandomMultiThreaded() {
        runMultiThreaded { testCoshRandom(10) }
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testCoshUnlimitedFail() {
        BigDecimal.valueOf(2).cosh(MathContext.UNLIMITED)
    }

    @Test
    fun testTanhRandom() {
        testTanhRandom(100)
    }

    private fun testTanhRandom(count: Int) {
        assertRandomCalculation(
            adaptCount(count),
            "tanh",
            { random -> random.nextDouble() * 100 - 50 }, { x: Double -> tanh(x) }
        ) { x, mathContext -> x.tanh(mathContext) }
    }

    @Test
    fun testTanhRandomMultiThreaded() {
        runMultiThreaded { testTanhRandom(10) }
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testTanhUnlimitedFail() {
        BigDecimal.valueOf(2).tanh(MathContext.UNLIMITED)
    }

    @Test
    fun testCothHighAccuracy() {
        // Result from wolframalpha.com: coth(1.234)
        val expected =
            toBigDecimal("1.185205324770926556048860223430792270329901540411285137873448359282652953527094889287726814715510048521019659319860676563297837519357735755222676888688329329110303974145134088876752919477109732168023596610148428505480869241099661164811509156002985143871723093089577983606565248689782796839398517745077805743517260787039640386735301278276086876372931112481082107564483405956328739694693730574938024717490194723853249114925227131778575014948071122840812952034872863104176945075471984392801178291333351353562325025835067985454487645827393165057275970686744920047997309198618696444874486121281651600092381786769980527201043162650354454070144174248097841732569004869651674629227052611951971499852906803162445370338547925467890388150946743902496473437710179269365962240697297233777642354604121931412626467105497562707506260133068364009228804583415867970900958121367348411835937666408175088231826400822030426266552")
        assertPrecisionCalculation(
            expected,
            { mathContext -> BigDecimal("1.234").coth(mathContext) },
            10
        )
    }

    @Test
    fun testCothRandom() {
        testCothRandom(100)
    }

    private fun testCothRandom(count: Int) {
        assertRandomCalculation(
            adaptCount(count),
            "tanh",
            { random -> random.nextDouble() * 100 - 50 },
            { v: Double -> coth(v) }
        ) { x, mathContext -> x.coth(mathContext) }
    }

    @Test
    fun testCothRandomMultiThreaded() {
        runMultiThreaded { testCothRandom(10) }
    }

    @Test(expectedExceptions = [UnsupportedOperationException::class])
    fun testCothUnlimitedFail() {
        BigDecimal.valueOf(2).coth(MathContext.UNLIMITED)
    }

    private fun coth(x: Double): Double {
        return cosh(x) / sinh(x)
    }

    @Test
    fun testSinAsinRandom() {
        assertRandomCalculation(
            adaptCount(1000),
            "x",
            "asin(sin(x))",
            { random, mathContext -> randomBigDecimal(random, mathContext) },
            { x, _ -> x }
        ) { x, mathContext -> x.sin(mathContext).asin(mathContext) }
    }

    @Test
    fun testCosAcosRandom() {
        assertRandomCalculation(
            adaptCount(1000),
            "x",
            "acos(cos(x))",
            { random, mathContext -> randomBigDecimal(random, mathContext) },
            { x, _ -> x }
        ) { x, mathContext -> x.cos(mathContext).acos(mathContext) }
    }

    @Test
    fun testTanAtanRandom() {
        assertRandomCalculation(
            adaptCount(1000),
            "x",
            "atan(tan(x))",
            { random, mathContext -> randomBigDecimal(random, mathContext) },
            { x, _ -> x }
        ) { x, mathContext -> x.tan(mathContext).atan(mathContext) }
    }

    @Test
    fun testCotAcotRandom() {
        assertRandomCalculation(
            adaptCount(1000),
            "x",
            "acot(cot(x))",
            { random, mathContext ->
                var r: BigDecimal
                do {
                    r = randomBigDecimal(random, mathContext)
                } while (r.compareTo(BigDecimal.ZERO) == 0)
                r
            },
            { x, _ -> x }
        ) { x, mathContext -> x.cot(mathContext).acot(mathContext) }
    }

    @Test(expectedExceptions = [ArithmeticException::class])
    fun testCotEqualZero() {
        BigDecimal.ZERO.cot(MC)
    }


    @Test
    fun testSinhAsinhRandom() {
        assertRandomCalculation(
            adaptCount(1000),
            "x",
            "asinh(sinh(x))",
            { random, mathContext -> randomBigDecimal(random, mathContext) },
            { x, _ -> x }
        ) { x, mathContext -> x.sinh(mathContext).asinh(mathContext) }
    }

    @Test
    fun testCoshAcoshRandom() {
        assertRandomCalculation(
            adaptCount(1000),
            "x",
            "acosh(cosh(x))",
            { random, mathContext -> randomBigDecimal(random, mathContext) },
            { x, _ -> x }
        ) { x, mathContext -> x.cosh(mathContext).acosh(mathContext) }
    }

    @Test
    fun testTanhAtanhRandom() {
        assertRandomCalculation(
            adaptCount(1000),
            "x",
            "atan(tan(x))",
            { random, mathContext -> randomBigDecimal(random, mathContext) },
            { x, _ -> x }
        ) { x, mathContext -> x.tanh(mathContext).atanh(mathContext) }
    }

    @Test
    fun testCothAcothRandom() {
        assertRandomCalculation(
            adaptCount(1000),
            "x",
            "acoth(coth(x))",
            { random, mathContext -> randomBigDecimal(random, mathContext).add(BigDecimal.valueOf(0.0000001)) },
            { x, _ -> x }
        ) { x, mathContext -> x.coth(mathContext).acoth(mathContext) }
    }

    @Test
    fun testPow2Random() {
        assertRandomCalculation(
            adaptCount(1000),
            "x*x",
            "pow(x,2)",
            { random, mathContext -> randomBigDecimal(random, mathContext) },
            { x, mathContext -> x.multiply(x, mathContext) }
        ) { x, mathContext -> x.pow(2, mathContext) }
    }

    @Test
    fun testSqrtPow2Random() {
        assertRandomCalculation(
            adaptCount(1000),
            "x",
            "pow(sqrt(x),2)",
            { random, mathContext -> randomBigDecimal(random, mathContext) },
            { x, _ -> x }
        ) { x, mathContext -> x.sqrt(mathContext).pow(2, mathContext) }
    }

    @Test
    fun testSqrtRootRandom() {
        val value2 = BigDecimal("2")
        assertRandomCalculation(
            adaptCount(1000),
            "sqrt(x)",
            "root(x, 2)",
            { random, mathContext -> randomBigDecimal(random, mathContext) },
            { x, mathContext -> x.sqrt(mathContext) }
        ) { x, mathContext -> x.root(value2, mathContext) }
    }

    @Test
    fun testRootPowRandom() {
        for (n in Arrays.asList(BigDecimal("0.1"), BigDecimal("1.0"), BigDecimal("2.1"), BigDecimal("1234.5678"))) {
            assertRandomCalculation(
                adaptCount(1000),
                "x",
                "pow(root(x, $n),$n)",
                { random, mathContext -> randomBigDecimal(random, mathContext) },
                { x, _ -> x }
            ) { x, mathContext -> x.root(n, mathContext).pow(n, mathContext) }
        }
    }

    @Test
    fun testLogExpRandom() {
        assertRandomCalculation(
            adaptCount(1000),
            "x",
            "log(exp(x))",
            { random, mathContext -> randomBigDecimal(random, mathContext) },
            { x, _ -> x }
        ) { x, mathContext -> x.exp(mathContext).log(mathContext) }
    }

    @Test
    fun testLog2PowRandom() {
        assertRandomCalculation(
            adaptCount(1000),
            "x",
            "log2(pow(2,x))",
            { random, mathContext -> randomBigDecimal(random, mathContext) },
            { x, _ -> x }
        ) { x, mathContext -> BigDecimal(2).pow(x, mathContext).log2(mathContext) }
    }

    @Test
    fun testLog10PowRandom() {
        assertRandomCalculation(
            adaptCount(1000),
            "x",
            "log10(pow(10,x))",
            { random, mathContext -> randomBigDecimal(random, mathContext) },
            { x, _ -> x }
        ) { x, mathContext -> BigDecimal.TEN.pow(x, mathContext).log10(mathContext) }
    }

    /*************************************************************************************/

    private fun randomBigDecimal(random: Random, mathContext: MathContext): BigDecimal {
        val digits = charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
        val stringNumber = StringBuilder(mathContext.precision)
        stringNumber.append("0.")
        for (i in 0 until mathContext.precision) {
            stringNumber.append(digits[random.nextInt(digits.size)])
        }
        return BigDecimal(stringNumber.toString(), mathContext)
    }


    private fun assertToBigDecimal(string: String) {
        val expected = BigDecimal(string)
        for (i in 2..9) {
            val actual = toBigDecimal(string, MathContext.UNLIMITED, i)
            assertTrue(expected.compareTo(actual) == 0, "toBigDecimal(_,_,$i) $expected compareTo $actual")
            assertEquals(expected, actual)
        }
    }

    private fun assertToBigDecimalThrows(string: String) {
        assertThrows(NumberFormatException::class.java) { BigDecimal(string) }
        assertThrows(NumberFormatException::class.java) {
            toBigDecimal(
                string,
                MathContext.UNLIMITED,
                1
            )
        }
    }

    private fun assertPrecisionCalculation(
        fnMcBd: FnMcBd,
        startPrecision: Int,
        endPrecision: Int
    ) {
        val expected: BigDecimal = fnMcBd(MathContext(endPrecision * 2))
        assertPrecisionCalculation(expected, fnMcBd, startPrecision, endPrecision)
    }

    private fun assertPrecisionCalculation(
        expected: BigDecimal,
        fnMcBd: FnMcBd,
        startPrecision: Int
    ) {
        assertPrecisionCalculation(expected, fnMcBd, startPrecision, expected.precision() - 20)
    }

    private fun assertPrecisionCalculation(
        expected: BigDecimal,
        fnMcBd: FnMcBd,
        startPrecision: Int,
        endPrecision: Int
    ) {
        var precision = startPrecision
        while (precision <= endPrecision) {
            val mathContext = MathContext(precision)
            assertBigDecimal(
                "precision=$precision",
                expected.round(mathContext),
                fnMcBd(mathContext),
                mathContext
            )
            precision += getPrecisionStep()
        }
    }

    private fun getPrecisionStep(): Int {
        return when (TEST_LEVEL) {
            TestLevel.Fast -> 50
            TestLevel.Medium -> 20
            TestLevel.Slow -> 5
        }
    }

    private fun toCheck(value: Double): BigDecimal? {
        val longValue = value.toLong()
        if (value == longValue.toDouble()) {
            return BigDecimal.valueOf(longValue)
        }
        return if (java.lang.Double.isFinite(value)) {
            BigDecimal.valueOf(value)
        } else null
    }

    private fun toCheck(value: BigDecimal): BigDecimal {
        return BigDecimal.valueOf(value.round(MC_CHECK_DOUBLE).toDouble())
    }

    private fun assertBigDecimal(expected: BigDecimal, actual: BigDecimal, mathContext: MathContext): Boolean {
        return assertBigDecimal("assertBigDecimal", expected, actual, mathContext)
    }

    private fun assertBigDecimal(
        description: String,
        expected: BigDecimal,
        actual: BigDecimal,
        mathContext: MathContext
    ): Boolean {
        val calculationMathContext = MathContext(mathContext.precision + 10)
        val error = expected.subtract(actual, calculationMathContext).abs()
        val acceptableError = actual.round(mathContext).ulp()
        val fullDescription =
            "$description expected=$expected : actual=$actual : precision=$mathContext.precision : error=$error : acceptableError=$acceptableError"
        assertTrue(error <= acceptableError, fullDescription)
        return error.signum() == 0
    }

    private fun assertThrows(exceptionClass: Class<out Exception?>, runnable: Runnable): Exception? {
        return assertThrows(exceptionClass, null, runnable)
    }

    private fun assertThrows(exceptionClass: Class<out Exception?>, message: String?, runnable: Runnable): Exception? {
        val result: Exception? /* = java.lang.Exception? */
        try {
            runnable.run()
            fail("Expected: $exceptionClass.name")
        } catch (exception: Exception) {
            if (!exceptionClass.isAssignableFrom(exception.javaClass)) {
                fail("Expected: $exceptionClass.name")
            }
            if (message != null && message != exception.message) {
                fail("Expected: $exceptionClass.name with message: `$message` but received message: `$exception.message`")
            }
            result = exception
        }
        return result
    }

    private fun getTestLevel(): TestLevel {
        var level = TestLevel.Fast
        val envTestLevel = System.getenv("BIGDECIMALTEST_LEVEL")
        if (envTestLevel != null) {
            try {
                level = TestLevel.valueOf(envTestLevel)
            } catch (ex: IllegalArgumentException) {
                System.err.println("Illegal env var TEST_LEVEL: $envTestLevel")
            }
        }
        return level
    }


    private fun assertRandomCalculation(
        count: Int,
        functionName: String,
        xFunction: FnRndDbl,
        doubleFunction: FnDblDbl?,
        calculation: FnBdMcBd
    ) {
        val random = Random(1)
        for (i in 0 until count) {
            val precision = random.nextInt(RANDOM_MAX_PRECISION) + 1
            val xDouble: Double = xFunction(random)
            val x = BigDecimal.valueOf(xDouble)
            val description = "$functionName($x)"
            // println("Testing $description precision=$precision")
            val mathContext = MathContext(precision)
            val result = calculation(x, mathContext)
            if (doubleFunction != null && precision > MC_CHECK_DOUBLE.precision + 4) {
                val doubleResult: BigDecimal? = toCheck(doubleFunction(xDouble))
                if (doubleResult != null) {
                    val doubleDescription = "$description vs. double function "
                    assertBigDecimal(doubleDescription, doubleResult, result, MC_CHECK_DOUBLE)
                }
            }
            val referenceMathContext = MathContext(precision * 2 + 20)
            val referenceResult = calculation(x, referenceMathContext)
            val expected = referenceResult.round(mathContext)
            assertBigDecimal(description, expected, result, mathContext)
        }
    }

    private fun assertRandomCalculation(
        count: Int,
        functionName: String,
        xFunction: FnRndDbl,
        yFunction: FnRndDbl,
        doubleFunction: FnDblDblDbl?,
        calculation: FnBdBdMcBd
    ) {
        val random = Random(1)
        for (i in 0 until count) {
            val precision = random.nextInt(100) + 1
            val xDouble: Double = xFunction(random)
            val yDouble: Double = yFunction(random)
            val x = BigDecimal.valueOf(xDouble)
            val y = BigDecimal.valueOf(yDouble)
            val description = "$functionName($x,$y)"
            // println("Testing $description precision=$precision")
            val mathContext = MathContext(precision)
            val result: BigDecimal = calculation(x, y, mathContext)
            if (doubleFunction != null && precision > MC_CHECK_DOUBLE.precision + 4) {
                val doubleResult = toCheck(doubleFunction(xDouble, yDouble))
                val doubleDescription = "$description vs. double function : $result"
                assertBigDecimal(doubleDescription, doubleResult!!, result, MC_CHECK_DOUBLE)
            }
            val expected: BigDecimal = calculation(x, y, MathContext(precision + 20, mathContext.roundingMode))
            assertBigDecimal(description, expected, result, mathContext)
        }
    }

    private fun assertRandomCalculation(
        count: Int,
        function1Name: String,
        function2Name: String,
        xFunction: FnRndMcBd,
        calculation1: FnBdMcBd,
        calculation2: FnBdMcBd
    ) {
        val random = Random(1)
        for (i in 0 until count) {
            val numberPrecision = random.nextInt(100) + 1
            val calculationPrecision = numberPrecision + 10
            val numberMathContext = MathContext(numberPrecision)
            val x = xFunction(random, numberMathContext)
            val calculationMathContext = MathContext(calculationPrecision)
            val y1 = calculation1(x, calculationMathContext)
            val y2 = calculation2(x, calculationMathContext)
            val description = "x=$x $function1Name=$y1 $function2Name=$y2"
            // println("Testing $description precision=$numberPrecision")
            assertBigDecimal(description, y1, y2, numberMathContext)
        }
    }

    private fun getMaxPrecision(): Int {
        return when (TEST_LEVEL) {
            TestLevel.Fast -> 100
            TestLevel.Medium -> 200
            TestLevel.Slow -> 1000
        }
    }

    private fun adaptCount(count: Int): Int {
        return when (TEST_LEVEL) {
            TestLevel.Fast -> count
            TestLevel.Medium -> count * 10
            TestLevel.Slow -> count * 100
        }
    }

    private fun getRangeStep(step: Double): Double {
        return when (TEST_LEVEL) {
            TestLevel.Fast -> step
            TestLevel.Medium -> step / 2
            TestLevel.Slow -> step / 10
        }
    }

}

typealias FnMcBd = (MathContext) -> BigDecimal
typealias FnRndDbl = (Random) -> Double
typealias FnDblDbl = (Double) -> Double
typealias FnBdMcBd = (BigDecimal, MathContext) -> BigDecimal
typealias FnDblDblDbl = (Double, Double) -> Double
typealias FnRndMcBd = (Random, MathContext) -> BigDecimal
typealias FnBdBdMcBd = (BigDecimal, BigDecimal, MathContext) -> BigDecimal